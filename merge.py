import pandas as pd
import numpy as np
import os


def bagging():
    src_folder_path = '/media/dell/delldisk/dell/wxm/Data/KaggleDDD/merge/src'
    res_folder_path = '/media/dell/delldisk/dell/wxm/Data/KaggleDDD/merge/res'
    src_path_1 = src_folder_path + '/' + 'e-1_Md-VGG_16_E-5_BS-16_CR-False_CC-True_Aug-False_ConTi-False_L-categorical_crossentropy_CF-softmax.csv'
    src_path_2 = src_folder_path + '/' + 'submission_multicrop_googlenet_step_6epoch_iter_49200_2016-06-09-16-44.csv'
    src_path_3 = src_folder_path + '/' + 'submission_multicrop_resnet152_step_iter_49200_2016-06-10-12-58.csv'
    res_path = res_folder_path + '/' + 'vgg045plusgoogle051plusres152056.csv'

    src1 = np.asarray(pd.read_csv(src_path_1))
    src2 = np.asarray(pd.read_csv(src_path_2))
    src3 = np.asarray(pd.read_csv(src_path_3))
    src1 = np.asarray(sorted(src1, key=lambda e: e[10]))
    src2 = np.asarray(sorted(src2, key=lambda e: e[10]))
    src3 = np.asarray(sorted(src3, key=lambda e: e[10]))

    img_names1 = src1[:, 10]
    img_names2 = src2[:, 10]
    img_names3 = src3[:, 10]
    res_1 = src1[:, 0:10]
    res_2 = src2[:, 0:10]
    res_3 = src3[:, 0:10]

    for i in range(len(img_names1)):
        if img_names1[i] != img_names2[i] != img_names3[i]:
            print 'False'
    res = (res_1 + res_2 + res_3) / 3.

    result = pd.DataFrame(res, columns=['c0', 'c1', 'c2', 'c3', 'c4', 'c5', 'c6', 'c7', 'c8', 'c9'])
    result.loc[:, 'img'] = pd.Series(img_names1, index=result.index)
    result.to_csv(res_path, index=False)


bagging()
