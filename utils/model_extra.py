import numpy as np
import keras.backend as K

def log_loss_c(y_true, y_pred):
    return K.mean(K.sum(-y_true * K.log(1e-20 + y_pred), axis=1))