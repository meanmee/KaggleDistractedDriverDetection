#!/usr/bin/en sh
DATA=/media/dell/delldisk/dell/Rui/data/Kaggle/DistractedDriverDetection/fine-tune-caffe
IMAGE_ROOT=/media/dell/delldisk/dell/Rui/data/Kaggle/DistractedDriverDetection/imgs/train/
ROOT=/media/dell/delldisk/dell/Rui/data/Kaggle/DistractedDriverDetection/imgs/
BACKEND='lmdb'
rm -rf $DATA/data_lmdb/driver_sorted/25vs1/img_train_9cropped_multicrop_224_${BACKEND}
rm -rf $DATA/data_lmdb/driver_sorted/25vs1/img_val_9cropped_multicrop_224_${BACKEND}
/home/dell/caffe-master/build/tools/convert_imageset --resize_height=224 --resize_width=224 \
$ROOT $DATA/data_txt/driver_sorted/25vs1/multi_crop/train_list_9cropped.txt  $DATA/data_lmdb/driver_sorted/25vs1/img_train_9cropped_multicrop_224_${BACKEND} --backend=${BACKEND}

/home/dell/caffe-master/build/tools/convert_imageset \
--resize_height=224 --resize_width=224 \
$ROOT $DATA/data_txt/driver_sorted/25vs1/multi_crop/val_list_9cropped.txt  $DATA/data_lmdb/driver_sorted/25vs1/img_val_9cropped_multicrop_224_${BACKEND} --backend=${BACKEND}
