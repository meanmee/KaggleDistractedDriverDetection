#!/usr/bin/env sh
#/home/dell/caffe-master/build/tools/caffe train -solver /home/dell/Rui/kaggle_ddd_finetune_caffe/fine_tune_googlenet/solver_multistep.prototxt -weights /media/dell/delldisk/dell/Rui/pretrined_model/caffe/googlenet/imagenet_googlenet.caffemodel
LOGDIR=/home/dell/Rui/kaggle_ddd_finetune_caffe/fine_tune_resnet101/25vs1/log

/home/dell/caffe-master/build/tools/caffe train -solver /home/dell/Rui/kaggle_ddd_finetune_caffe/fine_tune_resnet101/25vs1/solver_step.prototxt -weights /media/dell/delldisk/dell/Rui/pretrined_model/caffe/ResNet/ResNet-101-model.caffemodel 2>&1 | tee $LOGDIR/log_resnet101_3epoch_9crop.txt
