#!/usr/bin/en sh
DATA=/media/lab/labdisk/home/lab1314/xryu/data/DistractedDriverDetection/fine-tune-caffe
IMAGE_ROOT=/media/lab/labdisk/home/lab1314/xryu/data/DistractedDriverDetection/imgs/train/
BACKEND='lmdb'
rm -rf $DATA/data_lmdb/driver_sorted/25vs1/img_train_cropped_224_${BACKEND}
rm -rf $DATA/data_lmdb/driver_sorted/25vs1/img_val_cropped_224_${BACKEND}
/home/lab/wjSun/caffe-master/build/tools/convert_imageset \
--resize_height=224 --resize_width=224 \
$IMAGE_ROOT $DATA/data_txt/driver_sorted/25vs1/train_list.txt  $DATA/data_lmdb/driver_sorted/25vs1/img_train_cropped_224_${BACKEND} --backend=${BACKEND}

/home/lab/wjSun/caffe-master/build/tools/convert_imageset \
--resize_height=224 --resize_width=224 \
$IMAGE_ROOT $DATA/data_txt/driver_sorted/25vs1/val_list.txt  $DATA/data_lmdb/driver_sorted/25vs1/img_val_cropped_224_${BACKEND} --backend=${BACKEND}


