#!/usr/bin/en sh
DATA=/media/lab/labdisk/home/lab1314/xryu/data/DistractedDriverDetection/fine-tune-caffe
BACKEND='lmdb'
rm -rf $DATA/googlenet_data/img_train_${BACKEND}
rm -rf $DATA/googlenet_data/img_test_${BACKEND}
/home/lab/wjSun/caffe-master/build/tools/convert_imageset \
--resize_height=256 --resize_width=256 \
/ $DATA/data_txt/train.txt  $DATA/googlenet_data/img_train_${BACKEND} --backend=${BACKEND}

/home/lab/wjSun/caffe-master/build/tools/convert_imageset \
--resize_height=256 --resize_width=256 \
/ $DATA/data_txt/test.txt  $DATA/googlenet_data/img_test_${BACKEND} --backend=${BACKEND}


