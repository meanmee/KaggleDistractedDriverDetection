#!/usr/bin/en sh
DATA=/media/dell/delldisk/dell/Rui/data/Kaggle/DistractedDriverDetection/fine-tune-caffe
IMAGE_ROOT=/media/dell/delldisk/dell/Rui/data/Kaggle/DistractedDriverDetection/imgs/train/
BACKEND='lmdb'
RESIZE_SIZE=224
rm -rf $DATA/data_lmdb/driver_sorted/img_train_cropped_${BACKEND}
rm -rf $DATA/data_lmdb/driver_sorted/img_val_cropped_${BACKEND}
/home/dell/caffe-master/build/tools/convert_imageset \
--resize_height=$RESIZE_SIZE --resize_width=$RESIZE_SIZE \
$IMAGE_ROOT $DATA/data_txt/driver_sorted/train_list_cropped.txt  $DATA/data_lmdb/driver_sorted/img_train_cropped_${BACKEND} --backend=${BACKEND}

/home/dell/caffe-master/build/tools/convert_imageset \
--resize_height=$RESIZE_SIZE --resize_width=$RESIZE_SIZE \
$IMAGE_ROOT $DATA/data_txt/driver_sorted/val_list_cropped.txt  $DATA/data_lmdb/driver_sorted/img_val_cropped_${BACKEND} -backend ${BACKEND}

