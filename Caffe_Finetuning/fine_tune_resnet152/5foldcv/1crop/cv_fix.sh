#!/usr/bin/env sh
LOGDIR=/home/dell/Rui/kaggle_ddd_finetune_caffe/fine_tune_googlenet/5foldcv/log
TOOLS=/home/dell/Rui/caffe/build/tools

for i in 0
do 
	#rm -rf $LOGDIR/5fold_cv_${i}.txt
	$TOOLS/caffe train -solver /home/dell/Rui/kaggle_ddd_finetune_caffe/fine_tune_googlenet/5foldcv/1crop/5epoch/solver_fix_cv_${i}.prototxt -weights /media/wjsun/delldisk/dell/Rui/pretrined_model/caffe/googlenet/imagenet_googlenet.caffemodel 2>&1 | tee $LOGDIR/1crop/fix_cv_${i}_train32_val64_lr_1e-4.txt 
done
#python /home/dell/Rui/kaggle_ddd_finetune_caffe/kfold_cv_caffe.py -solver_type fix 2>&1 | tee $LOGDIR/1crop/classification_fix_cv_lr_1e-4.txt  
