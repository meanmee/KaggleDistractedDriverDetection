import os
import importlib
import sys
import cv2
import numpy as np
from skimage import io
import h5py
import tensorflow as tf

def test_path():
    print os.path.join('ex', 'exx')


def import_module():
    print importlib.import_module('experiments/linear'.replace('/', '.') + '.' + 'model.py'.replace('.py', ''))


def sys_float_max():
    print  sys.float_info.max


def return_args():
    x = 1
    y = 2
    return x, y


def tuple_ix():
    a = (0, 1)
    print a[0]


def img_size_to_arr_size():
    img_path = '/media/dell/cb552bf1-c649-4cca-8aca-3c24afca817b/dell/wxm/Data/KaggleDistractedDriverDetection/train/c0/img_34.jpg'
    img_arr = cv2.imread(img_path)
    print img_arr.shape
    img_arr_r = cv2.resize(img_arr, (80, 60))
    print img_arr_r.shape
    cv2.imshow('i', img_arr_r)
    cv2.waitKey(0)


def pring_format():
    print '{0}, {1}'.format(0, 1)


def driver_data():
    dr = dict()
    path = '/media/dell/cb552bf1-c649-4cca-8aca-3c24afca817b/dell/wxm/Data/KaggleDistractedDriverDetection/driver_imgs_list.csv'
    print('Read drivers data')
    f = open(path, 'r')
    line = f.readline()
    while (1):
        line = f.readline()
        if line == '':
            break
        arr = line.strip().split(',')
        dr[arr[2]] = arr[0]
    f.close()
    driver_id = dr.values()
    print driver_id
    unique_drivers = sorted(list(set(driver_id)))
    print unique_drivers


def check_str():
    a = 'lll'
    if a:
        print 0


def np_row_stack():
    a = np.asarray([1, 2, 1])
    b = np.asarray([1, 3, 3])
    a = np.row_stack([a, b])

    a = [1, 2, 2, 2, 1]
    c = [1, 2, 3, 4, 1]
    b = []
    b += a[0:2]
    b += c[0:2]
    print b


def check_cv2():
    train_path = '/media/dell/cb552bf1-c649-4cca-8aca-3c24afca817b/dell/wxm/Data/KaggleDDD/train/c1'
    for img in os.listdir(train_path):
        img_path = train_path + '/' + img
        img_arr = cv2.imread(img_path)
        print img_arr.shape


def check_walk():
    train_path = '/media/dell/cb552bf1-c649-4cca-8aca-3c24afca817b/dell/wxm/Data/KaggleDDD/train'
    for current_fold, sub_folds, sub_files in os.walk(train_path):
        print current_fold
        print sub_folds
        print sub_files


def check_list():
    for xy_offset in [[0, 0], [32, 0], [16, 16], [0, 32], [32, 32]]:
        print xy_offset[1]


def check_tf_imread():
    train_path = '/media/dell/cb552bf1-c649-4cca-8aca-3c24afca817b/dell/wxm/Data/KaggleDDD/train/c1'
    for img in os.listdir(train_path):
        img_path = train_path + '/' + img
        img_arr = cv2.imread(img_path)
        print img_arr.shape

def check_h5_weights():
    weights_path = '/media/dell/cb552bf1-c649-4cca-8aca-3c24afca817b/dell/wxm/Data/KaggleDDD/experiments/224x224/baseline/' \
                    'M-VGG_16_E-3_BS-128_CR-False_CC-True_Aug-False_ConTi-False_L-categorical_crossentropy_CF-softmax.h5'
    # weights_path = '/media/dell/cb552bf1-c649-4cca-8aca-3c24afca817b/dell/ExData/vgg16.h5'
    f = h5py.File(weights_path)
    # print f['conv1_1'].attrs
    # print f['layer_1'].attrs['nb_params']
    print f.keys()

if __name__ == '__main__':
    check_h5_weights()
