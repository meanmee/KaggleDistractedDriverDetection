from utils.Fea_Ext_Models import VGG_16_Extractor, VGG_16_finetuned_Extractor
from ex import set_configs
# import tensorflow as tf
from DataUtils.Load_util import load_local_train_val, load_local_train_val_DEBUG
import keras.backend as K
from skimage import io
import cv2
import numpy as np


def Extract_VGG_16_feats():
    image_narrays = None
    lable_narrays = None
    drivers_id = None
    unique_drivers = None
    config, _ = set_configs()
    config['debug'] = 'True'
    config['weights_path'] = 'vgg16.h5'
    config['crop_center'] = 'True'
    if config['debug'] == 'False' and config['Data_Mode_Tr'] == 'single_scale':
        image_narrays, lable_narrays, drivers_id, unique_drivers = load_local_train_val(config=config)
    elif config['debug'] == 'True' and config['Data_Mode_Tr'] == 'single_scale':
        image_narrays, lable_narrays, drivers_id, unique_drivers = load_local_train_val_DEBUG(config=config,
                                                                                              num_samples=10)
    print 'image loaded'
    model = VGG_16_finetuned_Extractor(which_feats='FC7')
    model.compile('adam', 'mse')

    X_feats = model.predict(image_narrays)
    print X_feats.shape
    print drivers_id
    print unique_drivers
    np.save('/media/dell/cb552bf1-c649-4cca-8aca-3c24afca817b/dell/wxm/Data/KaggleDDD/npy/VGG_FT_FC7_X.npy', X_feats)
    np.save('/media/dell/cb552bf1-c649-4cca-8aca-3c24afca817b/dell/wxm/Data/KaggleDDD/npy/VGG_FT_FC7_y.npy', lable_narrays)
    np.save('/media/dell/cb552bf1-c649-4cca-8aca-3c24afca817b/dell/wxm/Data/KaggleDDD/npy/VGG_FT_FC7_did.npy', drivers_id)
    np.save('/media/dell/cb552bf1-c649-4cca-8aca-3c24afca817b/dell/wxm/Data/KaggleDDD/npy/VGG_FT_FC7_ud.npy', unique_drivers)


if __name__ == '__main__':
    Extract_VGG_16_feats()
